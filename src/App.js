import './App.css';
import Navbar from './components/Navbar';
import Hero from './components/Hero';
import Card from './components/Card';
import cards from './data';

function App() {
    const elements = cards.map(card => {
        return <Card
            key={card.id}
            {...card}
        />
    })

  return (
    <div className='wrapper'>
        <Navbar />
        <Hero />
        <section className='cards-list'>{elements}</section>
    </div>
  );
}

export default App;
