
export default function Navbar(){
    return (
        <nav className='navbar'>
            <img className='logo' src='./images/logo.png' alt='airbnb-logo' />
        </nav>
    )
}
