
function HeroDescription(){
    return (
        <div className='hero-desc'>
            <h1 className='hero-heading'>Online Experiences</h1>
            <p className='hero-text'>Join unique interactive activities led by one-of-a-kind hosts-all without leaving home.</p>
        </div>
    )
}

export default function Hero(){
    return (
        <section className='hero'>
            <img className='hero-image' src='./images/photo-collage.png' alt='collage' />
            <HeroDescription />
        </section>
    )
}
