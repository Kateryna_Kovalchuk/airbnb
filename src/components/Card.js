
export default function Card(props){
    let text;
    if (!props.openSpots){
        text = 'SOLD OUT'
    } else if (props.location === 'Online'){
        text = 'ONLINE'
    }
    console.log(props)

    return(
        <div className='card'>
            <img src={`./images/${props.coverImg}`} alt='card' className='card-image' />
            <div>
                <div><span className='card-star'>★</span> {props.stats.rating} <span className='color-grey'>({props.stats.reviewCount})</span> &#183; <span className='color-grey'>{props.location}</span></div>
                <div>{props.title}</div>
                <div><span className='card-price'>From ${props.price}</span> / person</div>
            </div>
            {text && <div className='card-status'>{text}</div>}
        </div>
    )
}
